import Vue from 'vue'
import Vuetify from 'vuetify'
import VueRouter from 'vue-router'
import App from './App.vue'

import './stylus/main.styl'


import vuttiLayout from './layout/Layout.vue'
import vuttiMenu from './layout/Menu.vue'
import vuttiToolbar from './layout/Toolbar.vue'

import vuttiDashboard from './routes/dashboard.vue'
import vuttiSettings from './routes/settings.vue'
import vuttiMessages from './routes/messages.vue'
import vuttiLogin from './routes/login.vue'

Vue.use(VueRouter)
Vue.use(Vuetify)

Vue.component('vutti-layout', vuttiLayout)
Vue.component('vutti-menu', vuttiMenu)
Vue.component('vutti-toolbar', vuttiToolbar)

const api_url = "https://api.trito.lt/api/";


const UsersRoute = { template: '<div>Users</div>' }
const NotificationRoute = { template: '<div>Notification</div>' }
const HistoryRoute = { template: '<div>History</div>' }

const routes = [
    { path: '/', component: vuttiDashboard},
    { path: '/Login', component: vuttiLogin  },
    { path: '/Dashboard', component: vuttiDashboard },
    { path: '/Users', component: UsersRoute },
    { path: '/Settings', component: vuttiSettings },
    { path: '/Messages', component: vuttiMessages },
    { path: '/Notification', component: NotificationRoute },
    { path: '/History', component: HistoryRoute },
]

// configure router
const router = new VueRouter({
    routes, // short for routes: routes
    linkActiveClass: 'active'
})

new Vue({
    el: '#app',
    router,
    render: h => h(App)
})